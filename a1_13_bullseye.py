from decimal import *
import math
from sys import *

script, fileName = argv

reader = open(fileName)
nCases = int(reader.readline())
getcontext().prec = 100

for i in xrange(0, nCases):
    r, B = [Decimal(x) for x in reader.readline().split()]
    print "Case #" + str(i+1) + ":", math.trunc((-2*r + 1 + Decimal((2*r-1)**2 + 8*B).sqrt())/4)

reader.close()
