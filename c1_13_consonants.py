#! /usr/bin/python
# -*- coding: utf-8 -*-
from sys import *

script, filename = argv

reader = open(filename)
nCases = int(reader.readline())

for line in xrange(0, nCases):
    name, n = [x for x in reader.readline().split()]
    n = int(n)
    c = 0
    substringN = 0
    lastSubstring = 0
    lenName = len(name)
    for i in range(lenName):
        if name[i] not in 'aeiou':
            c = c + 1
        else:
            c = 0
        if c >= n:
            substringN = substringN + 1 - n - lastSubstring + lenName + ((i-n+1-lastSubstring)*(lenName-i-1))
            lastSubstring = i -n + 2

    print "Case #" + str(line +1) + ":", substringN
