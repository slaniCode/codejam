import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.math.BigInteger;

public class A1B_13_energy {

	private static int[] tasks;
	private static int[] energy;
	private static int MAX;
	
	private static BigInteger solverFast(int e, int r, int n)
	
	{
		BigInteger b = BigInteger.valueOf(e);
		BigInteger gain = BigInteger.valueOf(0);
		for (int i = 0; i < n; i++)
		{
			BigInteger a = BigInteger.valueOf(tasks[i]);
			gain = gain.add(a.multiply(b));
		}

		return gain;
	}

	private static BigInteger solver(int e, int r, int n)
	{
		MAX = e;
		energy = new int[n];
		energy[0] = e;

		for (int i = 1; i < n; i++)
		{
			if (tasks[i] > tasks[i-1])
			{
				energy[i] = r;
				manage(i, i-1);
			} else {
				energy[i] = r;
			}
		}

		BigInteger gain = BigInteger.valueOf(0);
		for (int i = 0; i < n; i++)
		{
			BigInteger a = BigInteger.valueOf(tasks[i]);
			BigInteger b = BigInteger.valueOf(energy[i]);
			gain = gain.add(a.multiply(b));
		}

		return gain;
	}

	public static void manage(int i, int j)
	{

		int dif = MAX - energy[i];
		if (dif >= energy[j])
		{
			energy[i] += energy[j];
			energy[j] -= energy[j];
		}
		else {
			energy[i] += dif;
			energy[j] -= dif;
		}

		if (energy[i] < MAX && tasks[i] > tasks[j-1]) {
			manage(i, j-1);
		}

		return;
	}

	public static void main(String args[])
	{
		int e, r, n;
		int nCases;
		String name;
		Scanner sc;
		
		try {
			sc = new Scanner(new File(args[0]));
			nCases = sc.nextInt();
			for (int i = 0; i < nCases; i++)
			{
				e = sc.nextInt();
				r = sc.nextInt();
				n = sc.nextInt();
				tasks = new int[n];
				for (int j = 0; j < n; j++)
				{
					tasks[j] = sc.nextInt();
				}

				int c = i + 1;
				if (r >= e) {
					System.out.println("Case #" + c + ": " + solverFast(e, r, n));
				} else {
					System.out.println("Case #" + c + ": " + solver(e, r, n));
				}
			}
		} catch (IOException c) {
			System.out.println(c);
		}
	}
}
