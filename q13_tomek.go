package main

import (
	"bufio"
	"fmt"
	"os"
)

const size = 4
const x, o, d uint8 = 'X', 'O', '.'

type TomekCase struct {
	dot                    bool
	xCol, xRow, oCol, oRow []bool
}

var nCases int

func main() {

	file, _ := os.Open(os.Args[1])
	reader := bufio.NewReader(file)
	line, _ := reader.ReadString('\n')
	nCases = readInt(line)

	for i := 0; i < nCases; i++ {
		tomek := new(TomekCase)
		tomek.initTomekCase()
		for r := 0; r < size; r++ {
			line, _ = reader.ReadString('\n')
			for c := 0; c < size; c++ {
				switch line[c] {
				case x:
					tomek.oCol[c] = true
					tomek.oRow[r] = true
					if c == r {
						tomek.oRow[size] = true
					}
					if c+r == 3 {
						tomek.oCol[size] = true
					}
				case o:
					tomek.xRow[r] = true
					tomek.xCol[c] = true
					if c == r {
						tomek.xRow[size] = true
					}
					if c+r == 3 {
						tomek.xCol[size] = true
					}
				case d:
					tomek.dot = true
					tomek.oCol[c] = true
					tomek.oRow[r] = true
					tomek.xRow[r] = true
					tomek.xCol[c] = true
					if c == r {
						tomek.xRow[size] = true
						tomek.oRow[size] = true
					}
					if c+r == 3 {
						tomek.xCol[size] = true
						tomek.oCol[size] = true
					}
				}
			}
		}
		tomek.output(i + 1)
		reader.ReadString('\n')
	}
}

func (tomek *TomekCase) output(i int) {

	for j := 0; j <= size; j++ {
		if !tomek.oRow[j] || !tomek.oCol[j] {
			fmt.Printf("Case #%d: O won\n", i)
			return
		} else if !tomek.xRow[j] || !tomek.xCol[j] {
			fmt.Printf("Case #%d: X won\n", i)
			return
		}
	}

	if tomek.dot {
		fmt.Printf("Case #%d: Game has not completed\n", i)
		return
	}
	fmt.Printf("Case #%d: Draw\n", i)
}

func readInt(line string) int {

	var i int
	fmt.Sscanf(line, "%d", &i)
	return i
}

func (tomek *TomekCase) initTomekCase() {

	tomek.xCol = make([]bool, size+1)
	tomek.xRow = make([]bool, size+1)
	tomek.oCol = make([]bool, size+1)
	tomek.oRow = make([]bool, size+1)
}
