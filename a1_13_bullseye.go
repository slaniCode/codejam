package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
)

func main() {

	file, _ := os.Open(os.Args[1])
	reader := bufio.NewReader(file)
	nCases := readInt(reader.ReadString('\n'))

	for i := 1; i <= nCases; i++ {
		r := float64(readInt64(reader.ReadString(' ')))
		color := float64(readInt64(reader.ReadString('\n')))
		nRings := (-2*r + 1  + math.Hypot((2*r - 1),  2 * math.Sqrt(2*color))) / 4
		
		fmt.Printf("Case #%d: %d\n", i, int64(math.Trunc(nRings)))
	}
}

func readInt(line string, e error ) int {

	var i int
	fmt.Sscanf(line, "%d", &i)
	return i
}

func readInt64 (line string, e error ) int64 {

	var i int64
	fmt.Sscanf(line, "%d", &i)
	return i
}
