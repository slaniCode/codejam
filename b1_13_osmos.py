#! /usr/bin/python
# -*- coding: utf-8 -*-
from sys import *

class AddMote:
    create = 0
    absorb = 0
    index = 0

def findSolution(size, nMotes, motes):

    addMotes = []
    nOfAdded = 0
    sumOfAdded = 0
    sumOfAbsorb = 0
    i = 0


    if size == 1:
        return nMotes

    while i < nMotes:
        #! če ga lahko pojem, ga pojem in gre do naslednjega
        if size > motes[i]:
            size += motes[i]
        else:
            # preverim koliko se povecam in koliko sem jih
            # moral dodati da sem lahko pojedo nasldenjega
            size, add = howMuchToadd(size, motes[i])
            # dodat jih moram vec kot se mi jih je ostalo
            if add >= (nMotes - i):
                # dodaj koliko si jih lahko pojedo na racun zadnjega dodajanja
                if nOfAdded > 0:
                    oldMote = addMotes[nOfAdded - 1]
                    oldMote.absorb = (i - oldMote.index)
                    sumOfAbsorb += oldMote.absorb
                break
            # dodaj novo stevilo motov
            else:
                sumOfAdded += add
                newMote = AddMote()
                newMote.create = add
                newMote.index = i
                addMotes.append(newMote)
                if nOfAdded > 0:
                    oldMote = addMotes[nOfAdded-1]
                    oldMote.absorb = i - oldMote.index
                    sumOfAbsorb += oldMote.absorb
                nOfAdded += 1
        i += 1

    if i == nMotes and nOfAdded > 0:
        oldMote = addMotes[nOfAdded-1]
        oldMote.absorb = i - oldMote.index
        sumOfAbsorb += oldMote.absorb

    nOperation = 0
    if i < nMotes:
        nOperation = nMotes - i

    while sumOfAdded > sumOfAbsorb:
        sumOfAdded -= addMotes[nOfAdded -1].create
        sumOfAbsorb -= addMotes[nOfAdded-1].absorb
        nOperation += addMotes[nOfAdded-1].absorb
        nOfAdded -= 1

    return nOperation + sumOfAdded

def howMuchToadd(size, motes):
    
    add = 0
    while size <= motes:
        size = (2*size)-1
        add += 1

    return size+motes, add

script, filename = argv
reader  = open(filename)
nCases = int(reader.readline())

for case in xrange(0, nCases):
    size, nMotes = [int(x) for x in reader.readline().split()]
    motes = []
    [motes.append(int(x)) for x in reader.readline().split()]
    motes.sort()
    print "Case #" + str(case +1) + ":", findSolution(size, nMotes, motes)
