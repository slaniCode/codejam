import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class C1_13_A_Consonants {

	private static long solver(int n, String name)
	{
		int con = 0;
		int len = name.length();
		int last = 1 + len;
		long subS = 0;
		boolean[] match = new boolean[123];
		match[97] = true;
		match[101] = true;
		match[105] = true;
		match[111] = true;
		match[117] = true;

		for (int i = len-1; i >= 0; i--) 
		{
			con = match[(int) name.charAt(i)] ? 0 : con+1;
			if (con >= n)
			{
				last = i + n;
			}
			// fancy dancy but slower!!!
			subS += 1 + len - last;
		}

		return subS;
	}

	public static void main(String args[])
	{
		int n;
		int nCases;
		String name;
		Scanner sc;
		
		try {
			sc = new Scanner(new File(args[0]));
			nCases = sc.nextInt();
			for (int i = 0; i < nCases; i++)
			{
				name = sc.next();
				n = sc.nextInt();
				int c = i + 1;
				System.out.println("Case #" + c + ": " + solver(n, name));
			}
		} catch (IOException e) {
			System.out.println(e);
		}
	}
}
